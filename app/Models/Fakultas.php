<?php

namespace App\Models;

use App\Models\Jurusan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Fakultas extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama_fakultas','jurusan_id'
    ];


    public function jurusan()
    {
        return $this->hasMany(Jurusan::class);
    }
}
