<?php

namespace App\Models;

use App\Models\Alamat;
use App\Models\Jurusan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Mahasiswa extends Model
{
    use HasFactory;
    protected $guarded = [
        'id'
    ];

    public function alamat()
    {
        return $this->belongsTo(Alamat::class);
    }

    public function jurusan()
    {
        return $this->hasMany(Jurusan::class);
    }

}
